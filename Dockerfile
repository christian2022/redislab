FROM redis
# take in account our customed configuration for redis.conf
ADD ./redis.conf /usr/local/etc/redis/redis.conf
# The commented lines bellow are just example in case we would like to save our date with persistent volume
# we will implement that VOLUME option whit the docker-compose.yml file
#VOLUME 
    # create redis-data directory
#RUN mkdir redis-data
    # Change working dir to ./redis-data
#WORKDIR ./redis-date
#VOLUME  /data
EXPOSE 6379
CMD [ "redis-server", "/usr/local/etc/redis/redis.conf" ] 
